﻿using UnityEngine;
using System.Collections;

public class dockingControl : MonoBehaviour {

	public float percentageDockingSpeed = .25f;
	private Transform finishPoint;
	private bool exited;
	private fighterControl fc;
	private pilotBehavior pilot;
	public float marginExit;


	// Use this for initialization
	void Start () {
		finishPoint = transform.FindChild("finishPoint");
		exited = false;
		fc = GameObject.Find ("fighter").GetComponent<fighterControl>();
		pilot = GameObject.Find ("First Person Controller").GetComponent<pilotBehavior>();
	}
	
	void OnTriggerEnter(Collider col){

		if(col.tag == "Fighter" && exited){
			Debug.Log ("docking!");
			col.rigidbody.velocity = new Vector3(0,0,0);
			col.transform.rotation = Quaternion.Euler(0,0,45);
		}
	}

	void OnTriggerStay(Collider col){
		if(col.tag == "Fighter" && exited){

			//this logic needs to be solidified, it's okay for now, but it really needs work
			col.transform.rotation = Quaternion.Lerp(col.transform.rotation, fc.getStartRot(), percentageDockingSpeed);
			col.transform.position = Vector3.Lerp(col.transform.position, finishPoint.position, percentageDockingSpeed);
			if (dockingFinished (col)) {
				onDockingComplete();
			}		
		}

	}

	void OnTriggerExit(Collider col){

		if(col.tag == "Player"){
			Debug.Log ("docking control on trigger exit " + col.tag);
			exited = true;
			fc.resetFighter();
		}
	}

	void onDockingComplete(){
		Debug.Log ("docking complete");
		exited = false;
		fc.powerDown();
		fc.pilotDisembark ();
		pilot.leaveCockpit();
	}

	bool dockingFinished(Collider fighter){
		//(col.transform.position == finishPoint.position) && (col.transform.rotation == fc.getStartRot ())
//		Debug.Log ("dockingFinished? x:" + Mathf.Abs(finishPoint.position.x - fighter.transform.position.x) + " y:" + Mathf.Abs(finishPoint.position.y - fighter.transform.position.y) + " z:" + Mathf.Abs(finishPoint.position.z - fighter.transform.position.z));
		if(Mathf.Abs(finishPoint.position.x - fighter.transform.position.x) <= .4f && Mathf.Abs(finishPoint.position.y - fighter.transform.position.y) <= .5f && Mathf.Abs(finishPoint.position.z - fighter.transform.position.z) <= .09f){
			Debug.Log ("docking finished enough");
			return true;
		}
		return false;
	}

	bool isFinishedDocking(Collider fighter){
		//col.transform.position == finishPoint.position && col.transform.rotation == fc.getStartRot ()
		return true;
	}
}
