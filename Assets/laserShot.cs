﻿using UnityEngine;
using System.Collections;

public class laserShot : MonoBehaviour {
	public float ttl;
	private float lived;
	// Use this for initialization
	void Start () {
		lived = 0;
	}
	
	// Update is called once per frame
	void Update () {
		lived++;
		lived = lived * Time.deltaTime;
		if(lived >= ttl)
			Destroy(gameObject);
	}
}
