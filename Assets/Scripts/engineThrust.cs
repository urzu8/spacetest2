﻿using UnityEngine;
using System.Collections;

public class engineThrust : MonoBehaviour {

	float Ver = 0.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Ver = -Input.GetAxis ("leftStickVertical");//forward/back
		rigidbody.AddRelativeForce(new Vector3(0,0,Ver) * 1000.0f * 1.0f, ForceMode.Impulse);
	}
}
