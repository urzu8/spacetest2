﻿using UnityEngine;
using System.Collections;

public class pilotBehavior : MonoBehaviour {

	public Animator cf;//DO NOT LEAVE THIS HERE
	private GameObject fps;
	// Use this for initialization
	void Start () {
		fps = GameObject.Find("First Person Controller");
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Y button")){
			leaveCockpit ();
		}
	}

	public void enterCockpit(){
		//disable components
		fps.gameObject.GetComponent<FPSInputController>().enabled = false;
		fps.gameObject.GetComponent<CharacterMotor>().enabled = false;
		fps.gameObject.GetComponent<CharacterController>().enabled = false;
	}

	public void leaveCockpit(){
		Debug.Log ("leaving cockpit");
		cf.SetTrigger ("liftCockpit");
		fps.gameObject.GetComponent<FPSInputController>().enabled = true;
		fps.gameObject.GetComponent<CharacterMotor>().enabled = true;
		fps.gameObject.GetComponent<CharacterController>().enabled = true;
		//enable components
		//adjust position? to get out of cockpit?
	}
}
