﻿using UnityEngine;
using System.Collections;

public class fighterControl : MonoBehaviour {

	public float thrustSpeed = 3000; //how fast the X1 speed is
	public float reverseThrust;//not used yet
	public float horizontalRotationThrust;
	public float verticalRotationThrust;
	private int speedMod = 1; //modifier for base speed(thrustSpeed)
	public int rollThrust = 3000;

	public int rateOfFire = 1000;
	private float waitTime;
	private float currTime;

	public float fireRate = .5f;
	private float nextFire = 0f;


	public Transform pilot;
	public Transform seat;

	private bool rotMode = false;

	//thrusters
	public GameObject right_top_thruster;
	public GameObject right_bottom_thruster;
	public GameObject left_top_thruster;
	public GameObject left_bottom_thruster;

	private Quaternion initRot;
	private Vector3 initPos;

	public GameObject spawnPoint;
	public Rigidbody laser;
	public float fireSpeed;

	public Animator cockpitFloor;
	public BoxCollider cockpitTrigger;

	// Use this for initialization
	void Start () {
//		rigidbody.centerOfMass = new Vector3(0, 0, 0);
		GameObject engine = GameObject.Find ("engine");
		rigidbody.centerOfMass = engine.transform.localPosition;
		initRot = transform.rotation;//maybe localRotation?
		initPos = transform.localPosition;

		//init gunfire
		waitTime = 1f/((float)rateOfFire/60f);
		currTime = 0f;
	}

	public Quaternion getStartRot(){
		return initRot;
	}

	public void powerUp(){
		enabled = true;
		rigidbody.isKinematic = false;
		pilot.transform.parent = seat;
		pilot.transform.localPosition = new Vector3(0,0,0);

	}

	public void resetFighter(){
		cockpitTrigger.enabled = true;
	}

	public void powerDown(){
		Debug.Log ("powering down");

		cockpitTrigger.enabled = false;
		rigidbody.isKinematic = true;
		Animator animator = transform.gameObject.GetComponent<Animator>();
		animator.SetTrigger("openCanopy");
		seat.DetachChildren();
	}
	
	// Update is called once per frame
	void Update () {
		float afterburn = Input.GetAxis ("rightTrigger");
		if (afterburn <= 0) {
				afterburn = 0;
		}

		float fireWeapon = Input.GetAxis ("leftTrigger");
		if (fireWeapon > 0) {
			fireWeapon = 0;
		}
		float Ver = -Input.GetAxis ("leftStickVertical");//forward/back
		float rightStickHor = Input.GetAxis ("JoystickHorizontal");//left/right
		float Hor = Input.GetAxis("leftStickHorizontal");//rolling
		float rightStickVert = Input.GetAxis ("JoystickVertical");//roll nose forward/back
		if(Input.GetButtonUp ("L3")){
			if(rotMode){
				rotMode = false;
			}
			else{
				rotMode = true;
			}
		}

		if(rotMode){
			rigidbody.AddRelativeTorque(new Vector3(0, 0, -Hor) * horizontalRotationThrust, ForceMode.Impulse);
		}
		else{

			rigidbody.AddRelativeForce(new Vector3(Hor ,0,0) * thrustSpeed, ForceMode.Impulse);
		}
		float dt = 1f;
		if(afterburn >= .95)
			dt = Time.deltaTime;
		rigidbody.AddRelativeForce(new Vector3(0,0,afterburn) * thrustSpeed * afterburn, ForceMode.Impulse);//was Ver
		rigidbody.AddRelativeForce(new Vector3(0,Ver,0) * thrustSpeed, ForceMode.Impulse);//was Ver

		//rotate left/right
		rigidbody.AddRelativeTorque(new Vector3(0, rightStickHor, 0) * horizontalRotationThrust, ForceMode.Impulse);

		rigidbody.AddRelativeTorque(new Vector3(rightStickVert, 0, 0) * verticalRotationThrust, ForceMode.Impulse);


//		float triggers = Input.GetAxis("triggers");
		if(Input.GetButton("faceButtonX")){
		}
		if((Input.GetAxis("Mouse ScrollWheel") > 0)&& speedMod <= 3){
			speedMod++;
		}
		if(Input.GetAxis("Mouse ScrollWheel") < 0 && speedMod > 0){
			speedMod--;
		}
		if(Input.GetKey (KeyCode.Tab)){
			normalizeShip();
		}
		if(Input.GetButton("rightBumper") && rigidbody.velocity.z > 0){

			rigidbody.AddRelativeForce(new Vector3(0,0,-1) * thrustSpeed * 2f, ForceMode.Impulse);//was Ver
		}
		if(fireWeapon < 0){
			fireCurrentPrimary();
		}

	}

	public void pilotDisembark(){
		//get the cockpit_floor_collider animator and trigger lift cockpi floor or whatever

		cockpitFloor.SetTrigger("liftCockpit");
		//reset ship to defaults....powerDown?
	}

	private void fireCurrentPrimary(){
		currTime += Time.deltaTime;

		if(currTime > waitTime){
//			Debug.Log ("Firing! currTime: " + currTime + " , waitTime: " + waitTime + " = " + 1/(rateOfFire/60));
//			Debug.Log ("firing primary weapon" + Time.deltaTime);
			Transform spawn = transform.Find ("body_container/gun/shotSpawn");
			Rigidbody newShot  = (Rigidbody) Instantiate(laser, spawn.position, transform.rotation * Quaternion.Euler(90f, 0f, 0f));
			newShot.velocity = transform.forward * (fireSpeed + thrustSpeed)  * Time.deltaTime;//doesn't seem like this line is as intelligent as it needs to be

			currTime = 0f;
		}

	}

	public void normalizeShip(){
		//per update it will move towards 0, current, 0
		Quaternion rot = transform.rotation;
		Vector3 currVel = rigidbody.velocity;
		Vector3 noVel = rigidbody.velocity;
		if(rot != initRot){


			transform.rotation = Quaternion.RotateTowards(rot, initRot, 0.1f);

		}
		if(currVel != noVel){
			rigidbody.velocity = Vector3.Lerp(currVel, noVel, 0.1f);
		}
	}

	void addThrust(string dir){
		if(dir == "up")
			rigidbody.AddRelativeForce(new Vector3(0,0,-1) * thrustSpeed * speedMod, ForceMode.Impulse);
		if(dir == "down")
			rigidbody.AddRelativeForce(new Vector3(0,0,1) * thrustSpeed * speedMod);
		if(dir == "left")
			rigidbody.AddRelativeForce(new Vector3(-1,0,0) * thrustSpeed * speedMod);
		if(dir == "right")
			rigidbody.AddRelativeForce(new Vector3(1,0,0) * thrustSpeed * speedMod);
	}
}
