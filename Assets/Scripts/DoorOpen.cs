﻿using UnityEngine;
using System.Collections;

public class DoorOpen : MonoBehaviour {

	void OnTriggerEnter(Collider col) {
		Debug.Log("hey there, you trying to open this door?");
		if (col.gameObject.tag == "Player"){
			Animator animator = transform.parent.gameObject.GetComponent<Animator>();
			animator.SetTrigger("OpenDoor");
		}
		
	}
	void OnTriggerExit(Collider col){
		Debug.Log("Closing door.");
		Animator animator = transform.parent.gameObject.GetComponent<Animator>();
		animator.SetTrigger("CloseDoor");
	}
}
