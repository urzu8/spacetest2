﻿using UnityEngine;
using System.Collections;

public class cockpitEnter : MonoBehaviour {

	private GameObject pilot;
	private GameObject fighter;
	// Use this for initialization
	void Start () {
		pilot = GameObject.Find("First Person Controller");
		fighter = GameObject.Find("fighter");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Player"){
			col.gameObject.GetComponent<pilotBehavior>().enterCockpit();
			//col.gameObject.GetComponent<FPSInputController>().enabled = false;
			fighter.gameObject.GetComponent<fighterControl>().powerUp();
			Debug.Log("pilot has entered the cockpit.");
			Animator animator = fighter.gameObject.GetComponent<Animator>();
			animator.SetTrigger("closeCanopy");
		}
	}

	void OnTriggerExit(Collider col){
		Debug.Log ("exiting cockpit trigger");
		if (col.gameObject.tag == "Player") {

			pilot.GetComponent<pilotBehavior> ().leaveCockpit ();
		}
	}
}
