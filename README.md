# README #

This was a proof of concept for a much larger game idea. What I wanted to try was to create a first person controller that could transition into and out of a cockpit of a vehicle. Over about a week I managed to make a shaky proof of concept.

* Walk out to the dock and get into the red fighter.
* use a 360 controller to control fighter once inside
* clicking in the left stick switches between pitch/yaw control for the left stick
* there should be some code in there to make an oculus build